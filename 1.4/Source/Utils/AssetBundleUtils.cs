﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using Verse;

namespace WeatherSimulation
{
    /// <summary>
    /// 
    /// Pretty much copy paste from https://github.com/RealTelefonmast/TeleCore/blob/main/Source/TeleCore/Static/TeleContentDatabase.cs
    /// Thank you Maxim#7777 or Telefonmast or RealTelefonmast
    /// 
    /// </summary>

    [StaticConstructorOnStartup]
    public static class AssetBundleUtils
    {
        private static AssetBundle bundleInt;
        private static Dictionary<string, Shader> lookupShades;
        private static Dictionary<string, ComputeShader> lookupComputeShades;

        public static AssetBundle WeatherSimulationBundle
        {
            get
            {
                if (bundleInt == null)
                {
                    string pathPart = "";
                    if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                        pathPart = "osx";
                    else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                        pathPart = "win";
                    else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                        pathPart = "linux";

                    string mainBundlePath = Path.Combine(WeatherSimulationMod.Mod.Content.RootDir, $@"1.4\UnityAssets\{pathPart}\testbundle.unity3d");
                    bundleInt = AssetBundle.LoadFromFile(mainBundlePath);
                    Log.Message($"Loaded Bundle: {mainBundlePath}: {bundleInt != null}");
                }

                return bundleInt;
            }
        }

        public static readonly ComputeShader CalcTemp = LoadComputeShader("calcTemp");
        public static ComputeShader LoadComputeShader(string shaderName)
        {
            if (lookupComputeShades == null)
                lookupComputeShades = new Dictionary<string, ComputeShader>();
            if (!lookupComputeShades.ContainsKey(shaderName))
                lookupComputeShades[shaderName] = WeatherSimulationBundle.LoadAsset<ComputeShader>(shaderName);
            ComputeShader shader = lookupComputeShades[shaderName];
            if (shader == null)
            {
                Log.Warning($"Could not load shader '{shaderName}'");
                return null;
            }

            return shader;
        }

    }
}
