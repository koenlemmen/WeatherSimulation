﻿
using RimWorld.Planet;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherSimulation
{

    /// <summary>
    /// 
    /// This class is super important and does tons of preparation work for the simulations
    /// All calculations are only done once, thanks saving, though this does increase the save file with like 10MB
    /// Why is it saved in XML, I hate XML
    /// 
    /// TODO:
    /// - Storing the centers and recalculating the centers is probably no longer necessary because the Simulation Components only use LongLat and SeasonalShiftAmplitudes
    /// 
    /// </summary>
    public class WorldUtilsComp : WorldComponent
    {
        private WorldGrid grid;
        private List<Tile> tiles;

        private Task<List<Vector3>> calculateCentersTask;
        private List<Vector3> centers;
        public List<Vector3> Centers
        {
            get { return centers; }
        }

        private Task<List<Vector2>> calculateLatLongTask;
        private List<Vector2> longLat;
        public List<Vector2> LongLat
        {
            get { return longLat; }
        }

        private Task<List<float>> calculateSeasonalShiftAmplitudeTask;
        private List<float> seasonalShiftAmplitudes;
        public List<float> SeasonalShiftAmplitudes
        {
            get { return seasonalShiftAmplitudes; }
        }

        public WorldUtilsComp(World world) : base(world)
        {            
            Log.Message("[WeatherSimulation] initiated world utils comp, loading centers from save data or calculating them (this may take a while)");
            grid = this.world.grid;
            tiles = grid.tiles;

            ExposeData();

            if (centers == null)
            {
                calculateCentersTask = new Task<List<Vector3>>(calculateCenters);
                calculateCentersTask.Start();
            }

            if (longLat == null)
            {
                calculateLatLongTask = new Task<List<Vector2>>(calculateLongLat);
            }

            if (seasonalShiftAmplitudes == null)
            {
                calculateSeasonalShiftAmplitudeTask = new Task<List<float>>(calculateSeasonalShiftAmplitude);
            }
        }

        public bool utilsCompIsDone()
        {
            return (centers != null && longLat != null && seasonalShiftAmplitudes != null);
        }

        public override void WorldComponentUpdate()
        {
            if (centers != null && seasonalShiftAmplitudes != null && longLat != null) return;
            if (calculateCentersTask.IsCompleted && centers == null)
            {
                Log.Message("[WeatherSimulation] centers of tiles calculated");
                centers = calculateCentersTask.Result;
                calculateLatLongTask.Start();
            }
            if (calculateLatLongTask.IsCompleted && longLat == null)
            {
                Log.Message("[WeatherSimulation] longLat of tiles calculated");
                longLat = calculateLatLongTask.Result;
                calculateSeasonalShiftAmplitudeTask.Start();
            }
            if (calculateSeasonalShiftAmplitudeTask.IsCompleted && seasonalShiftAmplitudes == null && longLat != null)
            {
                Log.Message("[WeatherSimulation] seasonalShiftAmplitudes of tiles calculated");
                seasonalShiftAmplitudes = calculateSeasonalShiftAmplitudeTask.Result;
            }
        }

        private List<Vector3> calculateCenters()
        {
            return tiles.AsParallel().Select(tile => grid.GetTileCenter(tiles.IndexOf(tile))).ToList();
        }

        private List<Vector2> calculateLongLat()
        {
            return centers.AsParallel().Select(center =>
                new Vector2(Mathf.Atan2(center.x, -center.z) * 57.29578f, Mathf.Asin(center.y / 100f) * 57.29578f)
            ).ToList();

        }

        private List<float> calculateSeasonalShiftAmplitude()
        {
            return longLat.AsParallel().Select((longLat, i) =>
               longLat.y >= 0.0f ? TemperatureTuning.SeasonalTempVariationCurve.Evaluate(Mathf.Abs(centers[i].y / 100f)) : -TemperatureTuning.SeasonalTempVariationCurve.Evaluate(Mathf.Abs(centers[i].y / 100f))
            ).ToList();
        }

        public override void ExposeData()
        {
            base.ExposeData();

            if (Scribe.mode == LoadSaveMode.LoadingVars)
            {
                centers = new List<Vector3>();
                longLat = new List<Vector2>();
                seasonalShiftAmplitudes = new List<float>();
            }

            Scribe_Collections.Look(ref centers, "centers", LookMode.Value);
            Scribe_Collections.Look(ref longLat, "longLat", LookMode.Value);
            Scribe_Collections.Look(ref seasonalShiftAmplitudes, "seasonalShiftAmplitudes", LookMode.Value);
        }

    }
}
