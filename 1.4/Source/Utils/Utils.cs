﻿using System.Collections.Generic;
using System.Linq;

namespace WeatherSimulation
{
    public static class Utils
    {

        // I don't think I currently use any of this, but at some point I did, so I'm keeping it here for now

        /// <summary>
        ///
        /// Chunks a list into smaller sublists of a specified size.
        ///
        /// </summary>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <param name="source">The source list to be chunked.</param>
        /// <param name="chunkSize">The size of each chunk.</param>
        /// <returns>A list of sublists, where each sublist contains elements of the specified chunk size.</returns>
        public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }


        /// <summary>
        /// 
        /// Returns an array of divisors for a given number.
        /// 
        /// </summary>
        /// <param name="n">The number to find divisors for.</param>
        /// <returns>An array of integers representing the divisors of the input number.</returns>
        public static int[] getDivisors(int n)
        {
            List<int> res = new List<int>();
            int i = 1;
            while (i <= n)
            {
                if (n % i == 0)
                    res.Add(i);
                i++;
            }
            return res.ToArray();
        }

        /// <summary>
        ///
        /// Returns the closest divisor of a given number to a specified value.
        ///
        /// </summary>
        /// <param name="n">The number to find divisors for.</param>
        /// <param name="closeTo">The value to find the closest divisor to.</param>
        /// <returns>The closest divisor of the input number to the specified value.</returns>
        public static int getClosestSplit(int n, int closeTo)
        {
            int[] allDivisors = getDivisors(n);
            for(int i = 0; i < allDivisors.Length; i++)
            {
                if (closeTo < allDivisors[i])
                {
                    if (i == 0) return allDivisors[i];
                    if ((allDivisors[i] - closeTo) > (closeTo - allDivisors[i - 1])) return allDivisors[i - 1];
                    return allDivisors[i];
                }
            }
            return n;
        }
    }
}
