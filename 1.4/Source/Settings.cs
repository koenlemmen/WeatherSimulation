﻿using UnityEngine;
using Verse;

namespace WeatherSimulation
{

    /// <summary>
    /// 
    /// The Hello World of ModSettings
    /// I'll need this when I add settings
    /// 
    /// </summary>

    public class Settings : ModSettings
    {
        public bool setting = true;

        public void DoWindowContens(Rect wrect)
        {
            var options = new Listing_Standard();
            options.Begin(wrect);

            options.CheckboxLabeled("Sample setting", ref setting);
            options.Gap();

            options.End();
        }

        public override void ExposeData()
        {
            Scribe_Values.Look(ref setting, "settings", true);
        }
    }
}
