﻿using Verse;
using UnityEngine;
using HarmonyLib;

namespace WeatherSimulation
{
    public class WeatherSimulationMod : Verse.Mod
    {
        public static string ModName = "WeatherSimulation";
        private static Mod modInt;
        public static Mod Mod => modInt;

        public static Settings settings;

        public WeatherSimulationMod(ModContentPack content) : base(content)
        {
            modInt = this;
            Log.Message("Weather Simulation by TheRealLemon has booted");

            settings = GetSettings<Settings>();

#if DEBUG
            Harmony.DEBUG = true;
#endif

            Harmony harmony = new Harmony("TheRealLemon.WeatherSimulation");
            harmony.PatchAll();
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            settings.DoWindowContens(inRect);
        }
        
        public override string SettingsCategory()
        {
            return "WeatherSimulation";
        }
    }
}
