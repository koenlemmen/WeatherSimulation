﻿using RimWorld;
using RimWorld.Planet;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace WeatherSimulation
{
    public class TempSimulationComp : WorldComponent
    {
        // General vars
        private int lastCheck = -1;
        bool firstRun;
        int absTick;
        float calc1;

        // World vars
        private WorldGrid grid;
        WorldUtilsComp worldUtils;

        private int tilesCount;
        private List<Tile> tiles;

        // Calc temp
        private float yearPct;
        private float[] curTemps;
        private float[] temps;
        public float[] Temps
        {
            get { return temps; }
        }

        // Shader vars
        ComputeShader calcTempShader;
        int calcTempShaderKernelIndex;
        
        ComputeBuffer xCoordBuffer;
        ComputeBuffer tempsBuffer;
        ComputeBuffer seasonalShiftAmplitudesBuffer;

        ComputeBuffer curTempsBuffer;

        public TempSimulationComp(World world) : base(world)
        {
            // Set general vars
            firstRun = true;

            // Set world vars
            grid = world.grid;

            tilesCount = grid.TilesCount;
            tiles = grid.tiles;

            // Calc temp
            curTemps = new float[tilesCount];
            temps = new float[tilesCount];
            yearPct = Season.Winter.GetMiddleTwelfth(0f).GetBeginningYearPct();
        }

        public override void WorldComponentTick()
        {
            if (!world.GetComponent<WorldUtilsComp>().utilsCompIsDone())
            {
                return;
            }

            if (firstRun) firstRunFunc();

            if (lastCheck + 2500 <= Find.TickManager.TicksGame+100)
            {
                Log.Message("Update all temps");
                absTick = Find.TickManager.TicksAbs;
                calc1 = (float)(absTick / 60000 % 60);
                tempsBuffer.SetData(tiles.Select(tile => tile.temperature).ToArray());

                calcTempShader.SetFloat("absTick", absTick);
                calcTempShader.SetFloat("calc1", calc1);
                calcTempShader.SetFloat("yearPct", yearPct);

                calcTempShader.SetBuffer(calcTempShaderKernelIndex, "xCoordBuffer", xCoordBuffer);
                calcTempShader.SetBuffer(calcTempShaderKernelIndex, "tempsBuffer", tempsBuffer);
                calcTempShader.SetBuffer(calcTempShaderKernelIndex, "seasonalShiftAmplitudesBuffer", seasonalShiftAmplitudesBuffer);

                calcTempShader.SetBuffer(calcTempShaderKernelIndex, "curTempsBuffer", curTempsBuffer);

                calcTempShader.Dispatch(calcTempShaderKernelIndex, tilesCount/512+1, 1, 1);

                curTempsBuffer.GetData(curTemps);

                temps = curTemps.Clone() as float[];
                curTempsBuffer = new ComputeBuffer(tilesCount, sizeof(float));

                lastCheck = Find.TickManager.TicksGame;
            }
        }

        private void firstRunFunc()
        {
            worldUtils = world.GetComponent<WorldUtilsComp>();
            absTick = Find.TickManager.TicksAbs;
            calc1 = (float)(absTick / 60000 % 60);
            
            // Shader vars
            calcTempShader = AssetBundleUtils.LoadComputeShader("calcTemp");
            calcTempShaderKernelIndex = calcTempShader.FindKernel("CSMain");

            xCoordBuffer = new ComputeBuffer(tilesCount, sizeof(float));
            xCoordBuffer.SetData(worldUtils.LongLat.Select(longlat => longlat.x).ToArray());
            tempsBuffer = new ComputeBuffer(tilesCount, sizeof(float));
            tempsBuffer.SetData(tiles.Select(tile => tile.temperature).ToArray());
            seasonalShiftAmplitudesBuffer = new ComputeBuffer(tilesCount, sizeof(float));
            seasonalShiftAmplitudesBuffer.SetData(worldUtils.SeasonalShiftAmplitudes.ToArray());

            curTempsBuffer = new ComputeBuffer(tilesCount, sizeof(float));

            firstRun = false;
        }

    }
}
