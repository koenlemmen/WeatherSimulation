﻿using RimWorld;
using RimWorld.Planet;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using Verse;

namespace WeatherSimulation
{
    public class WindSimulationComp : WorldComponent
    {
        [StructLayout(LayoutKind.Sequential)]
        struct Entry
        {
            public Vector2 key;
            public bool value;
        }
        
        // General vars
        private int lastCheck = -1;
        bool firstRun;
        int absTick;

        // World vars
        private WorldGrid grid;
        WorldUtilsComp worldUtils;

        private int tilesCount;
        private List<Tile> tiles;

        // Calc pressure
        private Vector4[] curPressures;
        private float[] pressures;
        public float[] Pressures
        {
            get { return pressures; }
        }
        public float MinPressure
        {
            get { return pressures.Min(); }
        }
        public float MaxPressure
        {
            get { return pressures.Max(); }
        }

        // Shader vars
        ComputeShader calcPressureShader;
        int calcPressureShaderKernelIndex;
        
        ComputeBuffer xyLandbuffer; // Input
        struct xyLand
        {
            public float x;
            public float y;
            public float z;
        }

        ComputeBuffer curPressuresBuffer; // Output

        public WindSimulationComp(World world) : base(world)
        {
            // Set general vars
            firstRun = true;

            // Set world vars
            grid = world.grid;

            tilesCount = grid.TilesCount;
            tiles = grid.tiles;

            // Calc pressure
            curPressures = new Vector4[tilesCount];
        }

        public override void WorldComponentTick()
        {
            if (!world.GetComponent<WorldUtilsComp>().utilsCompIsDone())
            {
                return;
            }

            if (firstRun) firstRunFunc();

            if (lastCheck + 60000 <= Find.TickManager.TicksGame+1000)
            {
                Log.Message("Update all air pressure");

                calcPressureShader.SetFloat("range", 20);
                calcPressureShader.SetInt("xyLandCount", tilesCount);

                absTick = Find.TickManager.TicksAbs;
                // Use YearPercent * 60 to get the current date
                int day = (int)(GenDate.YearPercent(absTick, 0f) * 60);
                calcPressureShader.SetFloat("day", day);

                calcPressureShader.SetBuffer(calcPressureShaderKernelIndex, "xyLandBuffer", xyLandbuffer);

                calcPressureShader.SetBuffer(calcPressureShaderKernelIndex, "outputBuffer", curPressuresBuffer);

                calcPressureShader.Dispatch(calcPressureShaderKernelIndex, tilesCount / 512 + 1, 1, 1);

                curPressuresBuffer.GetData(curPressures);

                pressures = (curPressures.Clone() as Vector4[]).Select(v => v.x).ToArray();
                
                curPressuresBuffer = new ComputeBuffer(tilesCount, sizeof(float) * 4);

                // Log lowest and highest value from pressures
                // I'll remove this after more debugging LMAO
                Log.Message("Lowest pressure: " + pressures.Min());
                Log.Message("Highest pressure: " + pressures.Max());

                lastCheck = Find.TickManager.TicksGame;
            }
        }

        private void firstRunFunc()
        {
            worldUtils = world.GetComponent<WorldUtilsComp>();

            // Shader vars
            calcPressureShader = AssetBundleUtils.LoadComputeShader("calcPressureV2");
            calcPressureShaderKernelIndex = calcPressureShader.FindKernel("CSMain");

            xyLandbuffer = new ComputeBuffer(tilesCount, sizeof(float) * 3);
            
            xyLandbuffer.SetData(
                worldUtils.LongLat.Select(
                    (longlat, index) => new xyLand { 
                        x = longlat.x, 
                        y = longlat.y, 
                        z = tiles[index].biome != BiomeDefOf.Ocean && tiles[index].biome != BiomeDefOf.Lake ? 1 : 0 
                    }
                ).ToArray()
            );

            curPressuresBuffer = new ComputeBuffer(tilesCount, sizeof(float) * 4);

            firstRun = false;
        }

    }
}
