﻿using HarmonyLib;
using RimWorld;
using RimWorld.Planet;
using UnityEngine;

namespace WeatherSimulation
{

    /// <summary>
    /// 
    /// Temperature at oceans should vary more, so here I go, this patches that, thank you
    /// 
    /// </summary>
    
    [HarmonyPatch(typeof(WorldGenStep_Terrain), "GenerateTileFor")]
    public class WorldGenStep_Terrain_Patch
    {
        static void Postfix(ref Tile __result)
        {
            Tile tile = __result;
            if (tile.biome == BiomeDefOf.Ocean)
            {
                float t = Mathf.Abs(tile.elevation) / 1000f;
                tile.temperature = tile.temperature - Mathf.Lerp(2f, 10f, t);
            }
            __result = tile;
        }
    }
}
