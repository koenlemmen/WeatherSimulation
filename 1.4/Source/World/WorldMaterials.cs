﻿using UnityEngine;
using Verse;

namespace WeatherSimulation
{

    /// <summary>
    /// 
    /// I stole this from the decompiling Rimworld and made my own class exactly like it with blackjack and hookers and more importantly another color spectrum.
    /// 
    /// </summary>

    [StaticConstructorOnStartup]
    public static class WorldMaterials
    {
        private const int NumMats = 50;

        private static Material[] matsPressure;
        private static readonly Color[] PressureSpectrum = new Color[3]
        {
            new Color(0.0f, 0.0f, 1f, 0.5f),
            new Color(1.0f, 1.0f, 1.0f, 0.5f),
            new Color(1.0f, 0.0f, 0.0f, 0.5f)
        };

        static WorldMaterials()
        {
            WorldMaterials.GenerateMats(ref WorldMaterials.matsPressure, WorldMaterials.PressureSpectrum, WorldMaterials.NumMats);
        }

        private static void GenerateMats(ref Material[] mats, Color[] colorSpectrum, int numMats)
        {
            mats = new Material[numMats];
            for (int index = 0; index < numMats; ++index)
                mats[index] = MatsFromSpectrum.Get(colorSpectrum, (float)index / (float)numMats);
        }

        public static Material MatForPressure(float pressure, float minPressure, float maxPressure)
        {
            // Use you may use Mathf.FloorToInt, Mathf.Clamp, Mathf.InverseLerp
            int num = Mathf.FloorToInt(Mathf.InverseLerp(minPressure, maxPressure, pressure) * (float) WorldMaterials.NumMats);
            return WorldMaterials.matsPressure[Mathf.Clamp(num, 0, WorldMaterials.NumMats - 1)];
        }
    }
}
