﻿using RimWorld.Planet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Verse;

namespace WeatherSimulation
{

    /// <summary>
    /// 
    /// I'll probably only keep this in for debugging? Or maybe I'll add a weather machine that players can build to unlock the overlays.
    /// The overlays are not very fast because of drawing EVERY tile.
    /// 
    /// Switch weatherLayer to switch between the layers for debugging.
    /// 
    /// </summary>

    public class WorldLayer_Weather : WorldLayer
    {
        private string weatherLayer = "wind";

        private int checkInterval = 2500;
        private int lastCheck = -1;
        private bool firstRun;
        private bool secondRun;

        World world;
        WorldGrid grid;

        int tilesCount;
        List<Tile> tiles;
        List<int> tileIDToVerts_offsets;
        List<Vector3> verts;

        TempSimulationComp tempSimulationComp;
        WindSimulationComp windSimulationComp;

        public WorldLayer_Weather()
        {
            firstRun = true;
            secondRun = false;

            world = Find.World;
            grid = world.grid;
            
            tilesCount = grid.TilesCount;
            tiles = grid.tiles;
            tileIDToVerts_offsets = grid.tileIDToVerts_offsets;
            verts = grid.verts;

            if (weatherLayer == "wind")
            {
                checkInterval = 60000;
            }
        }

        public override bool ShouldRegenerate
        {
            get
            {
                if (!base.ShouldRegenerate && !Find.TickManager.Paused && lastCheck + checkInterval <= Find.TickManager.TicksGame)
                {
                    Log.Message("Update layer");
                    lastCheck = Find.TickManager.TicksGame;
                    return true;
                }
                return false;
            }
        }

        public override IEnumerable Regenerate()
        {
            foreach (object item in base.Regenerate())
            {
                yield return item;
            }

            if (world.GetComponent<WorldUtilsComp>().utilsCompIsDone() && weatherLayer == "temp")
            {
                if (firstRun)
                {
                    firstRun = false;
                    secondRun = true;
                    yield break;
                }
                if (secondRun)
                {
                    tempSimulationComp = Find.World.GetComponent<TempSimulationComp>();
                    secondRun = false;
                }

                for (int i = 0; i < tilesCount; i++)
                {
                    float elev = tiles[i].elevation;

                    float temp;
                    temp = tempSimulationComp.Temps[i];

                    int subMeshIndex;
                    Material mat = RimWorld.Planet.WorldMaterials.MatForTemperature(temp);
                    Color col = mat.color;
                    col.a = 0.5f;
                    mat.color = col;
                    LayerSubMesh subMesh = GetSubMesh(mat, out subMeshIndex);

                    int count = subMesh.verts.Count;
                    int num1 = 0;
                    int num2 = ((i + 1 < tileIDToVerts_offsets.Count) ? tileIDToVerts_offsets[i + 1] : verts.Count);
                    for (int j = tileIDToVerts_offsets[i]; j < num2; j++)
                    {
                        subMesh.verts.Add(verts[j]);
                        if (j < num2 - 2)
                        {
                            subMesh.tris.Add(count + num1 + 2);
                            subMesh.tris.Add(count + num1 + 1);
                            subMesh.tris.Add(count);
                        }
                        num1++;
                    }
                }

                FinalizeMesh(MeshParts.All);
            }

            if (world.GetComponent<WorldUtilsComp>().utilsCompIsDone() && weatherLayer == "wind")
            {
                if (firstRun)
                {
                    firstRun = false;
                    secondRun = true;
                    yield break;
                }
                if (secondRun)
                {
                    windSimulationComp = Find.World.GetComponent<WindSimulationComp>();
                    secondRun = false;
                }

                float minPressure = windSimulationComp.MinPressure;
                float maxPressure = windSimulationComp.MaxPressure;

                for (int i = 0; i < tilesCount; i++)
                {
                    float elev = tiles[i].elevation;

                    float temp;
                    temp = windSimulationComp.Pressures[i];

                    int subMeshIndex;
                    Material mat = WorldMaterials.MatForPressure(temp, minPressure, maxPressure);
                    Color col = mat.color;
                    col.a = 0.5f;
                    mat.color = col;
                    LayerSubMesh subMesh = GetSubMesh(mat, out subMeshIndex);

                    int count = subMesh.verts.Count;
                    int num1 = 0;
                    int num2 = ((i + 1 < tileIDToVerts_offsets.Count) ? tileIDToVerts_offsets[i + 1] : verts.Count);
                    for (int j = tileIDToVerts_offsets[i]; j < num2; j++)
                    {
                        subMesh.verts.Add(verts[j]);
                        if (j < num2 - 2)
                        {
                            subMesh.tris.Add(count + num1 + 2);
                            subMesh.tris.Add(count + num1 + 1);
                            subMesh.tris.Add(count);
                        }
                        num1++;
                    }
                }

                FinalizeMesh(MeshParts.All);
            }
        }

        

    }
}
